<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   
          <script type="text/javascript" src="<?php echo base_url(); ?>asset/Frontend/Darshtrip/js/jquery-1.11.1.min.js"></script>

<?php $this->load->view('Frontend/include/Header') ?>
<div class="slider">
    <ul id="hero-gallery" class="cS-hidden">
        <li data-thumb="<?php echo base_url(); ?>book-your-travel/images/uploads/slider4.jpg"> 
            <img src="<?php echo base_url(); ?>book-your-travel/images/uploads/slider4.jpg" alt="" />
        </li>
        <li data-thumb="<?php echo base_url(); ?>book-your-travel/images/uploads/slider2.jpg"> 
            <img src="<?php echo base_url(); ?>book-your-travel/images/uploads/slider2.jpg" alt="" />
        </li>
        <li data-thumb="<?php echo base_url(); ?>book-your-travel/images/uploads/slider3.jpg"> 
            <img src="<?php echo base_url(); ?>book-your-travel/images/uploads/slider3.jpg" alt="" />
        </li>
    </ul>
</div>
<div class="main-search" >
    <div class="wrap" >
                
          
                                    <form  id="main-search" action="<?php echo site_url() . '/flight/flight-search-view' ?>" method="post" id="main-search" name="hotel_form">

                                        <div class="row">
                                        <div class="col-md-6">
                        <label class="col-md-3" style="padding-left:0"><input type="radio" name="flight_journy_type" value="1" checked> One Way</label>
                        <label class="col-md-3"><input type="radio" name="flight_journy_type" value="2"> Round Way</label>
                        <label class="col-md-3"><input type="radio" name="flight_journy_type" value="3"> Multicity</label>
                    </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    
                                                    <input type="text" id="flight_search" class="input-text full-width flight_search" name="flight_search_from[]" placeholder="city, distirct or specific airpot" />
                                                    <input type="hidden" id="hdn_from_loc" class="hdn_from_loc" name="hdn_from_loc"/>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                
                                                    <input type="text" id="flight_to_location" name="flight_search_to[]" class="flight_to_location input-text full-width" placeholder="city, distirct or specific airpot" />
                                                    <input type="hidden" id="hdn_to_loc" class="hdn_to_loc" name="hdn_to_loc"/>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                      
                                                        <div class="datepicker-wrap ">
                                                            <input type="text" name="flight_date_from[]" class="input-text full-width" placeholder="dd/mm/yy" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 boxs round_way 2" id="date_to">
                                                       
                                                        <div class="datepicker-wrap">
                                                            <input type="text" name="flight_date_to[]" class="input-text full-width" placeholder="dd/mm/yy" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        <div class="col-md-2" style="z-index: 1; height:40px; text-alignt:left;">
                       
                                                   
                                                    <a class="btn btn-default btn-block show_person_list"  id="a_flight_travellers_count">(<span id="travelers_count">1</span>) Traveler(s) <i class="fa fa-angle-down"></i></a>

                                                    <input type="hidden" name="id" txt_flight_travellers_count="" id="txt_flight_travellers_count" value="1">
                                                    <div class="show_number_of_person" style="display:none;">
                                                        <div class="form-group">
                                                            <label>Adults (12+)</label>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                                                                        <span class="glyphicon glyphicon-minus"></span>
                                                                    </button>
                                                                </span>

                                                                <input type="text" name="flight_adults" id="quant[1]" class="form-control input-number travelers" readonly="readonly"  min="0" max="9" value="1">

                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                                                                        <span class="glyphicon glyphicon-plus"></span>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class=''>Children (2-11)</label>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default child_btn btn-number" disabled="disabled" data-type="minus" data-field="quant[2]">
                                                                        <span class="glyphicon glyphicon-minus"></span>
                                                                    </button>
                                                                </span>

                                                                <input type="text" name="flight_child" id="quant[2]" class="form-control input-number flight_child_number travelers" readonly="readonly"   min="0" max="2" value="0">

                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[2]">
                                                                        <span class="glyphicon glyphicon-plus"></span>
                                                                    </button>
                                                                </span>
                                                            </div>

                                                        </div>
                                                        <div class="form-group">
                                                            <label class=''>Infants (0-2)</label>
                                                            <div class="input-group">
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[3]">
                                                                        <span class="glyphicon glyphicon-minus"></span>
                                                                    </button>
                                                                </span>

                                                                <input type="text" name="flight_infant" id="quant[3]" class="form-control input-number travelers" readonly="readonly"  min="0" max="9" value="0">

                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[3]">
                                                                        <span class="glyphicon glyphicon-plus"></span>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="Child_age">

                                                        </div>
                                                        <h4 id="max_msg"></h4>
                                                        <h4 id="min_msg"></h4>
                                                        <button class="btn btn-block btn-danger show_person_list">Done</button>
                                                        <script type="text/javascript">
                                                            (function ($) {

                                                                $(document).ready(function () {
                                                                    $(".show_person_list").click(function (e) {
                                                                        e.preventDefault();
                                                                        $(".show_number_of_person").toggle();
                                                                    });
                                                                });
                                                                $(document).ready(function () {
                                                                    $('input[type="radio"]').on('ifChanged', function () {
                                                                        var inputValue = $(this).attr("value");
                                                                        var targetBox = $("." + inputValue);
                                                                        $(".boxs").not(targetBox).hide();
                                                                        $(targetBox).show();
                                                                    });
                                                                });
                                                                $('.btn-number').click(function (e) {
                                                                    e.preventDefault();
                                                                    fieldName = $(this).attr('data-field');
                                                                    type = $(this).attr('data-type');
                                                                    var input = $("input[id='" + fieldName + "']");
                                                                    var currentVal = parseInt(input.val());
                                                                    if (!isNaN(currentVal)) {
                                                                        if (type == 'minus') {

                                                                            if (currentVal > input.attr('min')) {
                                                                                input.val(currentVal - 1).change();
                                                                                var msg1 = '';
                                                                                document.getElementById("min_msg").innerHTML = msg1;
                                                                            }
                                                                            if (parseInt(input.val()) == input.attr('min')) {
                                                                                $(this).attr('disabled', true);
                                                                                var msg1 = 'Sorry, the minimum value was reached';
                                                                                document.getElementById("min_msg").innerHTML = msg1;
                                                                            }

                                                                        } else if (type == 'plus') {

                                                                            if (currentVal < input.attr('max')) {
                                                                                input.val(currentVal + 1).change();
                                                                                var msg2 = '';
                                                                                document.getElementById("max_msg").innerHTML = msg2;
                                                                            }
                                                                            if (parseInt(input.val()) == input.attr('max')) {
                                                                                $(this).attr('disabled', true);
                                                                                var msg2 = 'Sorry, the maximum value was reached';
                                                                                document.getElementById("max_msg").innerHTML = msg2;
                                                                            }

                                                                        }
                                                                    } else {
                                                                        input.val(0);
                                                                    }
                                                                });
                                                                $('.input-number').focusin(function () {
                                                                    $(this).data('oldValue', $(this).val());
                                                                });
                                                                $('.input-number').change(function () {

                                                                    minValue = parseInt($(this).attr('min'));
                                                                    maxValue = parseInt($(this).attr('max'));
                                                                    valueCurrent = parseInt($(this).val());
                                                                    name = $(this).attr('id');
                                                                    if (valueCurrent >= minValue) {
                                                                        $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
                                                                    } else {
                                                                        alert('Sorry, the minimum value was reached');
                                                                        $(this).val($(this).data('oldValue'));
                                                                    }
                                                                    if (valueCurrent <= maxValue) {
                                                                        $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
                                                                    } else {
                                                                        alert('Sorry, the maximum value was reached');
                                                                        $(this).val($(this).data('oldValue'));
                                                                    }

                                                                    var total = 0;
                                                                    $('.travelers').each(function (index, element) {
                                                                        total = total + parseFloat($(element).val());
                                                                    });
                                                                    $("#travelers_count").text(total);
                                                                });
                                                                $(".input-number").keydown(function (e) {
                                                                    // Allow: backspace, delete, tab, escape, enter and .
                                                                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                                                                            // Allow: Ctrl+A
                                                                                    (e.keyCode == 65 && e.ctrlKey === true) ||
                                                                                    // Allow: home, end, left, right
                                                                                            (e.keyCode >= 35 && e.keyCode <= 39)) {
                                                                                // let it happen, don't do anything
                                                                                return;
                                                                            }
                                                                            // Ensure that it is a number and stop the keyup
                                                                            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                                                                e.preventDefault();
                                                                            }
                                                                        });
                                                            })(jQuery);
                                                        </script>
                                                    </div>
                                              
                    </div> 
                                            <div class="col-md-1">
                                                  <button class="full-width icon-check">SEARCH NOW</button>
                                                
                                            </div>
                                              <div class="col-md-12">
                        <div class="column radios one-second" style="padding: 0px 0px 0px 0px; ">
                             <input type="checkbox" name="vehicle" value="non-stop"> 
                            <label for="no-stop">Non Stop Flight</label>
                        </div>
                    </div>
                                        </div>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function () {

                                                jQuery(document).on('keyup', '.flight_search', function () {
                                                    var from_airports = [];
                                                    var from_origen = [];
                                                    var search_param = jQuery(this).val();
                                                    if (search_param.length >= 1) {
                                                        jQuery.post('<?php echo site_url() . '/Flight/get_airport_list' ?>', {'search_param': search_param}).success(function (result) {
                                                            var res = JSON.parse(result);
                                                            jQuery.each(res, function (i, k) {
                                                                var city = res[i].city_name + ', ' + res[i].country_name + ' (' + res[i].aiport_code + ')';
                                                                from_airports.push(city);
                                                                from_origen.push(res[i].origin);
                                                            });
                                                            jQuery(".flight_search").autocomplete({
                                                                source: from_airports
                                                            });
                                                        });
                                                    }
                                                });
                                                jQuery('.flight_search').on('autocompletechange change', function () {
                                                    var from_airports = [];
                                                    var from_origen = [];
                                                    for (var i = 0; i < from_airports.length; i++)
                                                    {
                                                        if (from_airports[i] == this.value)
                                                        {
                                                            jQuery('.hdn_from_loc').val(from_origen[i]);
                                                        }
                                                    }
                                                }).change();
                                            });
                                        </script>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function () {
                                                var to_airports = [];
                                                var to_origen = [];
                                                jQuery(document).on('keyup', '.flight_to_location', function () {
                                                    var search_param = jQuery(this).val();
                                                    if (search_param.length >= 1) {
                                                        jQuery.post('<?php echo site_url() . '/Flight/get_airport_list' ?>', {'search_param': search_param}).success(function (result) {
                                                            var res = JSON.parse(result);
                                                            to_airports = [];
                                                            jQuery.each(res, function (i, k) {
                                                                var city = res[i].city_name + ', ' + res[i].country_name + ' (' + res[i].aiport_code + ')';
                                                                to_airports.push(city);
                                                                to_origen.push(res[i].origin);
                                                            });
                                                            jQuery(".flight_to_location").autocomplete({
                                                                source: to_airports
                                                            });
                                                        });
                                                    }
                                                });
                                                jQuery('.flight_to_location').on('autocompletechange change', function () {
                                                    //alert(this.value);
                                                    for (var i = 0; i < to_airports.length; i++)
                                                    {
                                                        if (to_airports[i] == this.value)
                                                        {
                                                            jQuery('.hdn_to_loc').val(to_origen[i]);
                                                        }
                                                    }
                                                    //jQuery('#tagsname').html('You selected: ' + this.value);
                                                }).change();
                                            });
                                        </script>
                                    </form>
                            

 
</div>
</div>
<?php $this->load->view('Frontend/include/Footer') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">


<script type="text/javascript" src="<?php echo base_url(); ?>asset/Frontend/Darshtrip/js/theme-scripts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/Frontend/Darshtrip/js/scripts.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="<?php echo base_url(); ?>asset/Backend/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script>
                                            (function ($) {
                                                $(document).ready(function () {
                                                    $('input').iCheck({
                                                        checkboxClass: 'icheckbox_square-orange',
                                                        radioClass: 'iradio_square-orange',
                                                        increaseArea: '20%' // optional
                                                    });

                                                });
                                            })(jQuery);
</script>
<style>
   .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
   position: relative;
   min-height: 1px;
    padding-right: 1px; 
    padding-left: 1px; 
}
   </style>
   

   
   <style>
   .form-group {
    margin-bottom: 0px; 
}
label{
   text-align: left;
   margin-top: 3px;
   display: inline-block;
   max-width: 100%;
   margin-bottom: -2px;
   font-weight: 700;
   }
   .show_number_of_person{
       background-color: #fff;
       padding: 10px;
   }
   </style>